﻿namespace AbelhinhaWebAPI.Patterns
{
    public interface IObserver<T>
    {

        void Update(T payload);

    }
}