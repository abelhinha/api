namespace AbelhinhaWebAPI.Patterns
{
    class VehicleMachine
    {

        // ESTADOS
        OpenState openState;
        CloseState closeState;

        // ESTADO ATUAL
        IState state;

        public VehicleMachine() {

            openState = new OpenState(this);
            closeState = new CloseState(this);

            state = closeState; // Inicialmente está fechado o veículo
        }

        public void OpenVehicle() {
            state.OpenVehicle();
        }

        public void CloseVehicle() {
            state.CloseVehicle();
        }

        public void SetState(IState aState) {
            this.state = aState;
        }

        public IState GetState() {
            return this.state;
        }

        public IState GetOpenState() {
            return this.openState;
        } 

        public IState GetCloseState() {
            return this.closeState;
        }


    }
}