﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models;

namespace AbelhinhaWebAPI.Patterns
{
    public class PaymentPendingState:PaymentState
    {
        public PaymentPendingState(Payment aPayment) : base(aPayment)
        {
        }

        public override void Paid()
        {
            base.Payment.ChangeState(new PaymentPaidState(base.Payment));
        }

        public override void Pending()
        {

        }

        public override void Error()
        {
            base.Payment.ChangeState(new PaymentErrorState(base.Payment));
        }

        public override string ToString()
        {
            return "Pendente";
        }
    }
}
