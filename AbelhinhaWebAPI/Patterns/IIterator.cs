namespace AbelhinhaWebAPI.Patterns
{
    public interface IIterator
    {

        bool HasNext();
        object Next();

    }
}