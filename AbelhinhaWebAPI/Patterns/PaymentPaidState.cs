﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models;

namespace AbelhinhaWebAPI.Patterns
{
    public class PaymentPaidState : PaymentState
    {


        public PaymentPaidState(Payment aPayment) : base(aPayment)
        {
        }

        public override void Paid()
        {
            return;

        }

        public override void Pending()
        {
            return;
        }

        public override void Error()
        {
            return;
        }

        public override string ToString()
        {
            return "Pago";
        }
    }
}
