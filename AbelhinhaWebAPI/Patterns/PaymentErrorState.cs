﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models;

namespace AbelhinhaWebAPI.Patterns
{
    public class PaymentErrorState:PaymentState
    {
        public PaymentErrorState(Payment aPayment) : base(aPayment)
        {
        }

        public override void Paid()
        {
            base.Payment.ChangeState(new PaymentPaidState(base.Payment));
        }

        public override void Pending()
        {
            base.Payment.ChangeState(new PaymentPendingState(base.Payment));

        }

        public override void Error()
        {
            return;
        }

        public override string ToString()
        {
            return "Erro";
        }
    }
}
