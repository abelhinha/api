using System;

namespace AbelhinhaWebAPI.Patterns
{
    class OpenState : IState
    {

        VehicleMachine machine = new VehicleMachine();

        public OpenState(VehicleMachine aMachine) {
            this.machine = aMachine;
        }

        public void OpenVehicle() {
            Console.WriteLine("Already opened");
        }

        public void CloseVehicle() {
            this.machine.SetState(this.machine.GetCloseState());
        }

    }
}