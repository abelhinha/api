namespace AbelhinhaWebAPI.Patterns
{
    public abstract class Extra : VehicleClass { 
    
        public abstract string Description { get; }

    }
}