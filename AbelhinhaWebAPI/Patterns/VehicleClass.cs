namespace AbelhinhaWebAPI.Patterns
{
    public abstract class VehicleClass
    {

        public string description = "";

        public virtual string Description {
            get { return description; }
        }

        public abstract double Cost(); 

    }
}