namespace AbelhinhaWebAPI.Patterns
{
    class CardsIterator : IIterator
    {

        private string[] cards;
        int position = 0;

        public CardsIterator(string[] aCards) {
            this.cards = aCards;
        }

        public bool HasNext()
        {
            if (position >= cards.Length || cards[position] == null)
                return false;
            return true;
        }

        public object Next() {
            string card = cards[position];
            position = position + 1;
            return card;
        }

    }
}