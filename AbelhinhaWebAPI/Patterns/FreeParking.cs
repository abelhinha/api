namespace AbelhinhaWebAPI.Patterns
{
    public class FreeParking : Extra {

        private VehicleClass vehicle;

        public FreeParking(VehicleClass aVehicle) {
            this.vehicle = aVehicle;
        }

        public override string Description {
            get {
                return vehicle.Description + " + Free Parking";
            }
        }

        public override double Cost() {
            return vehicle.Cost() + 40;
        }

    }
}