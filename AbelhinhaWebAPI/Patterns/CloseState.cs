using System;

namespace AbelhinhaWebAPI.Patterns
{
    class CloseState : IState
    {

        VehicleMachine machine = new VehicleMachine();

        public CloseState(VehicleMachine aMachine) {
            this.machine = aMachine;
        }

        public void OpenVehicle() {
            this.machine.SetState(this.machine.GetOpenState());
        }

        public void CloseVehicle() {
            Console.WriteLine("Already closed");
        }

    }
}