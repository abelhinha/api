namespace AbelhinhaWebAPI.Patterns
{
    public interface IState
    {

        // MÉTODOS DO STATE
        void OpenVehicle();
        void CloseVehicle();

    }
}