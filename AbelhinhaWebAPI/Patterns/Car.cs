namespace AbelhinhaWebAPI.Patterns
{
    public class Car : VehicleClass
    {

        public Car() {
            this.description = "Car";
        }

        public override double Cost() {
            return 80;
        }

    }
}