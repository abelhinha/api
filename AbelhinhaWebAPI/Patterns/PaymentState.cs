﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models;

namespace AbelhinhaWebAPI.Patterns
{
    public abstract class PaymentState
    {
        private Payment _payment;
        public Payment Payment => _payment;

        public PaymentState(Payment aPayment)
        {
            _payment = aPayment;
        }

        public abstract void Paid();

        public abstract void Pending();

        public abstract void Error();
        public abstract string ToString();
    }
}
