﻿namespace AbelhinhaWebAPI.Patterns
{
    public interface ISubject<T>
    {

        void Attach(IObserver<T> aObserver);
        void Detach(IObserver<T> aObserver);
    
    }
}