namespace AbelhinhaWebAPI.Patterns
{
    public class GPS : Extra {

        private VehicleClass vehicle;

        public GPS(VehicleClass aVehicle) {
            this.vehicle = aVehicle;
        }

        public override string Description {
            get {
                return vehicle.Description + " + GPS";
            }
        }

        public override double Cost() {
            return vehicle.Cost() + 15;
        }

    }
}