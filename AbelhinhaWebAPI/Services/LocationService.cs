﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace AbelhinhaWebAPI.Services
{
    public class LocationService
    {
        private static LocationService _instance;
        public HttpClient Client { get; }


        public static LocationService GetInstance()
        {
           return _instance ??= new LocationService();
        }

        private LocationService()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://eu1.locationiq.com/v1/reverse.php");
            // GitHub API versioning
            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            Client = client;
        }

        public async Task<string> GetAddress(double latitude, double longitude)
        {
            var response = await Client.GetAsync($"?key=c46ec0f3dba365&format=json&lat='{Math.Round(latitude,2)}'&lon='{Math.Round(longitude,2)}'").Result.Content.ReadAsStringAsync();
            //var response = await Client.GetStringAsync("?key=c46ec0f3dba365&format=json&lat=" + latitude.ToString() + "&lon=" + longitude.ToString());
            //response.EnsureSuccessStatusCode();
            //var responseStream = await response.Content.ReadAsStreamAsync();
            //var teste = await JsonSerializer.DeserializeAsync<IEnumerable<string>>(responseStream);
            
            JObject json = JObject.Parse(response);
            var address = json.GetValue("display_name");
            return address.ToString();
        }


    }
}
