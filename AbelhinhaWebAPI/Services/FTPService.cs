﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Renci.SshNet;

namespace AbelhinhaWebAPI.Services
{
    public class FTPService
    {
        private static FTPService _instance;

        private SftpClient _sftpConn;

        private FTPService()
        {
            _sftpConn = ConnectToSFTP();
        }

        public static FTPService GetInstance()
        {
            return _instance ??= new FTPService();
            //?? is the coalescent operation meaning that if _instance is not set it will do equal to the encryptionService
        }

        /// <summary>
        /// Adds an image to our CDN and 
        /// </summary>
        /// <param name="aFile"></param>
        /// <returns>Returns the url where it is available</returns>
        public List<string> PostImage(IFormFile[] aFiles)
        {

            List<string> locationsList = new List<string>();

            _sftpConn.Connect();

            foreach (var aFile in aFiles)
            {
                var ext = aFile.FileName.Split(".").Last();
                var newFileName = System.Guid.NewGuid().ToString();
                newFileName += "." + ext;
                addToFTP("./cdn/drivers/", aFile.OpenReadStream(), newFileName);
                locationsList.Add("/cdn/drivers/" + newFileName);
            }

            _sftpConn.Disconnect();

            return locationsList;
        }

        /// <summary>
        /// Adds a file to FTP
        /// </summary>
        /// <param name="location">Location where we want the file to be at the cdn</param>
        /// <param name="fs">A stream of the file we want to write.</param>
        /// <param name="filename">The name the new file should have</param>
        private void addToFTP(string location, Stream fs, string filename)
        {
            _sftpConn.UploadFile(fs, $"./cdn/drivers/{filename}", true);

            var finalLocation = location + "/";

        }

        /// <summary>
        /// Establishes the FTP connection
        /// </summary>
        /// <returns>Returns a usable connection to an FTP</returns>
        private SftpClient ConnectToSFTP()
        {
            var _host = Environment.GetEnvironmentVariable("CDN_HOST");
            var _user = Environment.GetEnvironmentVariable("CDN_USER");
            var _password = Environment.GetEnvironmentVariable("CDN_PWD");
            var _port = Convert.ToInt32(Environment.GetEnvironmentVariable("CDN_PORT"));

            var connectionInfo = new Renci.SshNet.ConnectionInfo(_host, _port, _user, new PasswordAuthenticationMethod(_user, _password));

            //template method aqui?
            /*using (var sftp = new SftpClient(connectionInfo))
            {
                sftp.Connect();
                using (var fileStream = System.IO.File.OpenRead(f)) -> esta parte seria um template method
                sftp.Disconnect();
            }*/
            return new SftpClient(connectionInfo);
        }
    }
}