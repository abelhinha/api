﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AbelhinhaWebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AbelhinhaWebAPI.Services
{
    public class AuthorizationService
    {
        private static AuthorizationService _instance;
        public static AuthorizationService GetInstance()
        {
            return _instance ??= new AuthorizationService();
        }

        public string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Environment.GetEnvironmentVariable("SECRET_TOKEN"));
            var tokenDescription = new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, Convert.ToString(user.ID)),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim("TRADEMARK","Este token tem um alto patrocínio da carolina patrocínio")
                }),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescription);
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Gets the users ID based on the claims present on JWT token.
        /// </summary>
        /// <param name="controllerBase">Receives the this argument of a controller</param>
        /// <returns>The ID of the user </returns>
        public int GetUserID(ControllerBase controllerBase)
        {
            
            var claimsIdentity = controllerBase.User.Identity as ClaimsIdentity;
            var userID = Convert.ToInt32(claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            return userID;
        }
    }
}
