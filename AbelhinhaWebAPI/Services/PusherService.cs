﻿using System;
using Environment = System.Environment;
using AbelhinhaWebAPI.Models;
using PusherServer;
namespace AbelhinhaWebAPI.Services
{
    /// <summary>
    /// A singleton pusher service.
    /// </summary>
    public class PusherService : Patterns.IObserver<NotificationModel>
    {
        private static PusherService _instance;
        private PusherOptions _options;
        private Pusher _pusher;

        private PusherService()
        {
            _options = new PusherOptions
            {
                Cluster = "eu",
                Encrypted = true
            };

            _pusher = new Pusher(
                Environment.GetEnvironmentVariable("PUSHER_APPID"),
                Environment.GetEnvironmentVariable("PUSHER_APPKEY"),
                Environment.GetEnvironmentVariable("PUSHER_APPSECRET"), 
                _options);
            

        }

        public static PusherService GetInstance()
        {
            return _instance ??= new PusherService();
        }

        /// <summary>
        /// Sends a message to a channel
        /// </summary>
        /// <param name="aChannel">The Channel the message should be sent to</param>
        /// <param name="aEvent">The event-name the client should listen to</param>
        /// <param name="aMessage">The message to be sent to the specified event</param>
        private async void SendPusherMessage(string aChannel, string aEvent, string aMessage)
        {
            await _pusher.TriggerAsync(aChannel,aEvent, new { message = aMessage });
        }

        public void Update(NotificationModel payload)
        {
            this.SendPusherMessage("notifications", payload.Type,payload.Notification);
        }
    }

}