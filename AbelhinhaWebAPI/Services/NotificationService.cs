﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Patterns;
using Microsoft.AspNetCore.Mvc;

namespace AbelhinhaWebAPI.Services
{
    public class NotificationService : ISubject<NotificationModel>
    {
        private List<IObserver<NotificationModel>> _observers = new List<IObserver<NotificationModel>>();

        private static NotificationService _instance;

        private NotificationService(){ }

        public static NotificationService GetInstance()
        {
            return _instance ??= new NotificationService();
        }

        public void SendNotification(string notificationType, string notificationContent)
        {
            foreach (var observer in _observers)
            {
                observer.Update(new NotificationModel
                {
                    Type = notificationType,
                    Notification = notificationContent
                });
            }
        }

        public void Attach(Patterns.IObserver<NotificationModel> aObserver)
        {
            _observers.Add(aObserver);
        }

        public void Detach(Patterns.IObserver<NotificationModel> aObserver)
        {
            _observers.Remove(aObserver);
        }

        
    }
}
