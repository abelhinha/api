﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Services
{
    /// <summary>
    ///     Singleton/Proxy class for providing phrase hashing using bcrypt;
    /// </summary>
    public class EncryptionService
    {
        private static EncryptionService _instance;
        private int _encryptionFactor;

        private EncryptionService()
        {
            _encryptionFactor = Convert.ToInt32(Environment.GetEnvironmentVariable("ENCRYPTION_FACTOR"));
        }
        
        /// <summary>
        /// Returns the Singletons instance
        /// </summary>
        /// <returns></returns>
        public static EncryptionService GetInstance()
        {
            return _instance ??= new EncryptionService(); 
            //?? is the coalescent operation meaning that if _instance is not set it will do equal to the encryptionService
        }

        /// <summary>
        /// Encripts a phrase using bcrypt;
        /// </summary>
        /// <param name="aPhrase">The string to be encrypted</param>
        /// <returns></returns>
        public string EncryptPhrase(string aPhrase)
        {
            return BCrypt.Net.BCrypt.HashPassword(aPhrase, workFactor: _encryptionFactor);
        }

        public bool VerifyEncriptedPhrase(string aPhrase, string aHashedPhrase)
        {
            //  aHashedPhrase = aHashedPhrase.Replace("\r\n", "");
            return BCrypt.Net.BCrypt.Verify(aPhrase, aHashedPhrase);
        }
    }
}
