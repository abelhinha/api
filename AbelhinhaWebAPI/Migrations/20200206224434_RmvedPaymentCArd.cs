﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbelhinhaWebAPI.Migrations
{
    public partial class RmvedPaymentCArd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "CardID",
                table: "Payments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments",
                column: "CardID",
                principalTable: "Cards",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "CardID",
                table: "Payments",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments",
                column: "CardID",
                principalTable: "Cards",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
