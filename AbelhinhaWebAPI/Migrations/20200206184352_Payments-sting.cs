﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbelhinhaWebAPI.Migrations
{
    public partial class Paymentssting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Rentals_RentalID",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "RentalID",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CardID",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments",
                column: "CardID",
                principalTable: "Cards",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Rentals_RentalID",
                table: "Payments",
                column: "RentalID",
                principalTable: "Rentals",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Rentals_RentalID",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "RentalID",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CardID",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Cards_CardID",
                table: "Payments",
                column: "CardID",
                principalTable: "Cards",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Rentals_RentalID",
                table: "Payments",
                column: "RentalID",
                principalTable: "Rentals",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
