﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbelhinhaWebAPI.Migrations
{
    public partial class testing_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "Payments",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Payments");
        }
    }
}
