﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Patterns;
using AbelhinhaWebAPI.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace AbelhinhaWebAPI.Controllers
{
    public class PaymentPutModel{
        
        public int CardID { get; set; }
        public int PaymentID { get; set; }

    }
    [Route("[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly AbelhinhaWebAPIContext _context;

        public PaymentsController(AbelhinhaWebAPIContext context)
        {
            _context = context;
        }

        // GET: Payments
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetPayments()
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);
            return await _context.Payments.Where(payment => (payment.Card.UserID == userID || payment.Rental.User.ID == userID) && payment.Status != "Pago")
                .Select(payment => new {
                    ID = payment.ID,
                    Rental = new
                    {
                        ID = payment.Rental.ID,
                        Vehicle = payment.Rental.Vehicle,
                        StartDate = payment.Rental.StartDate,
                        EndDate = payment.Rental.EndDate,
                    },
                    Price = payment.Price,
                    Status = payment.Status
                }).ToListAsync();
        }

        // GET: Payments/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<dynamic>> GetPayment(int id)
        {
            var payment = await _context.Payments.Where(payment => payment.ID == id).Select(payment => new {
                ID = payment.ID,
                Rental = new
                {
                    ID = payment.Rental.ID,
                    Vehicle = payment.Rental.Vehicle,
                    StartDate = payment.Rental.StartDate,
                    EndDate = payment.Rental.EndDate,
                },
                Price = payment.Price,
                Status = payment.Status
            }).ToListAsync();

            if (payment == null)
            {
                return NotFound();
            }

            return payment;
        }

        // PUT: Payments/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> FinishPayment([FromBody] PaymentPutModel payment)
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);
            var user = _context.Users.Find(userID);
            var userCard = user.Cards.Find(card => card.ID == payment.CardID);
            if (userCard == default(Card)) return BadRequest("NOT_OWNER_OF_CARD");
            var paymentToDB = _context.Payments.Find(payment.PaymentID);
            if (paymentToDB == default(Payment)) BadRequest("WRONG_ID");
            if (paymentToDB.Status == "Pago") return BadRequest("ALREADY_PAID");
            paymentToDB.Card = userCard;
            paymentToDB.Paid();
            paymentToDB.Status = paymentToDB.State.ToString(); //aldrabice porque não consigo persistir o state pattern na BD
            _context.Entry(paymentToDB).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentExists(payment.PaymentID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: Payments/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Payment>> DeletePayment(int id)
        {
            var payment = await _context.Payments.FindAsync(id);
            if (payment == null)
            {
                return NotFound();
            }

            _context.Payments.Remove(payment);
            await _context.SaveChangesAsync();

            return payment;
        }

        private bool PaymentExists(int id)
        {
            return _context.Payments.Any(e => e.ID == id);
        }
    }
}
