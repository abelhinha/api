﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Models.Validations;
using AbelhinhaWebAPI.Services;
using Microsoft.AspNetCore.Authorization;
using SQLitePCL;

namespace AbelhinhaWebAPI.Controllers
{

    /// <summary>
    /// This controller is used to manage all information about users. It is, it is responsible for providing all the methods that refers to users.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly AbelhinhaWebAPIContext _context;


        /// <summary>
        /// This constructor receives the context.
        /// </summary>
        /// <param name="context"></param>
        public UsersController(AbelhinhaWebAPIContext context)
        {
            _context = context;
        }



        /// <summary>
        /// This method is used to give a list of all the users of the system. Basically returns a list with all users.
        /// </summary>
        /// <returns>List of Users</returns>
        // GET: Users
        [HttpGet]
        //[Authorize(Roles = "employee,manager")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }



        /// <summary>
        /// This method is used to retrieve the user with the given id. Basically it returns the user with the id given in the params.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }




        /// <summary>
        /// This method is used to change the information of the user, giving in the params a new user to replace. The id is used to verify if he really exists.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        // PUT: Users/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.ID)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }




        /// <summary>
        /// This method is used to add a new user by sending the user in params.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        // POST: Users
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _context.Users.Add(user);
            user.Password = EncryptionService.GetInstance().EncryptPhrase(user.Password);
            await _context.SaveChangesAsync();
            //return CreatedAtAction("GetUser", new { id = user.ID }, user);
            return CreatedAtAction(nameof(GetUser), new { id = user.ID }, user);
        }



        /// <summary>
        /// Using this method, we can delete a user with a specific id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return user;
        }



        /// <summary>
        /// This method is used to get the email and password of the form and verify if the user is valid (if the data are correct and the user exists in the database)
        /// Also it creates a token and returns the token and the user email.
        /// </summary>
        /// <param name="AuthModel"></param>
        /// <returns>Email and Token of the User</returns>
        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] AuthenticationModel AuthModel)
        {
            var aEmail = AuthModel.Email;
            var aPassword = AuthModel.Password;
            var userFromDb = await _context.Users.SingleOrDefaultAsync(User => User.Email == aEmail);

            if (userFromDb == default(User))
                return Unauthorized(new { message = "INVALID_USER" });

            var isValid =  EncryptionService.GetInstance().VerifyEncriptedPhrase(aPassword, userFromDb.Password);

            if(!isValid) 
                return Unauthorized(new { message = "INVALID_PASSWORD" });

            var token = AuthorizationService.GetInstance().GenerateToken(userFromDb);
            userFromDb.Password = "";
            
            return new
            {
                email = userFromDb.Email,
                token = token
            };
        }



        /// <summary>
        /// This method is used to get the information from the form (email, password and username and documents). It uploads the files and saves the new user.
        /// </summary>
        /// <param name="ModelUser"></param>
        /// <returns></returns>
        [Route("register")]
        [HttpPost]
        public async Task<ActionResult<dynamic>> CreateAccount([FromForm] RegistrationModel ModelUser)
        {
            //if (!ModelState.IsValid) return "{'Error':'Invalid fields'}"; not needed he does by himself since we used required
            ModelUser.Password = EncryptionService.GetInstance().EncryptPhrase(ModelUser.Password);
            User realUser = new User
            {
                Email = ModelUser.Email,
                Password = ModelUser.Password,
                Username = ModelUser.Username
            };
            var driversDocuments = FTPService.GetInstance().PostImage(ModelUser.Files);

            List<FTPLocation> files = new List<FTPLocation>();
            //realUser.Token= TokenService.GetInstance().GenerateToken(realUser);
            int i = 0;
            foreach (var driversDocumentLocation in driversDocuments)
            {
                files.Add(new FTPLocation
                {
                    Location = driversDocumentLocation,
                    UID = realUser
                });

                _context.FtpLocations.Add(files[i]);
                i++;
            }
            //realUser.DriversDocuments = files;
            _context.Users.Add(realUser);
            await _context.SaveChangesAsync();
            var token = AuthorizationService.GetInstance().GenerateToken(realUser);
            return new
            {
                email = realUser.Email,
                token = token
            };
        }


        /// <summary>
        /// This method returns if the user with a specific id, already exists or not.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool UserExists(int id)
        { 
            return _context.Users.Any(e => e.ID == id);
        }
    }
}
