﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AbelhinhaWebAPI.Controllers
{

    /// <summary>
    /// This controller is used to manage all information about vehicles. It is, it is responsible for providing all the methods that refers to vehicles.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly AbelhinhaWebAPIContext _context;

        /// <summary>
        /// The constructor receives the context.
        /// </summary>
        /// <param name="context"></param>
        public VehiclesController(AbelhinhaWebAPIContext context)
        {
            _context = context;
        }


        /// <summary>
        /// This method returns a list of the existent vehicles.
        /// </summary>
        /// <returns>List of Vehicles</returns>
        // GET: Vehicles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetAvailableVehicles()
        {
            //var response = _context.Vehicles.Where(vehicle => IsVehicleAvailable(vehicle.ID)).ToList();
            var response = await _context.Vehicles.ToListAsync();
            var availableCars = new List<Vehicle>();
            foreach(var vehicle in response)
            {
                if (IsVehicleAvailable(vehicle.ID))
                    availableCars.Add(vehicle);
            }
            
            return Ok(availableCars);
            //return await _context.Vehicles.ToListAsync();
        }

        /// <summary>
        /// This method returns a specific vehicle, giving its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Vehicle</returns>
        // GET: Vehicles/{id}
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Vehicle>> GetVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);

            if (vehicle == null)
            {
                return NotFound();
            }

            return vehicle;
        }


        /// <summary>
        /// This method allows to change the information of a vehicle, giving the vehicle and the id. The id is used to verify if it already exists.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        // PUT: Vehicles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVehicle(int id, Vehicle vehicle)
        {
            if (id != vehicle.ID)
            {
                return BadRequest();
            }

            _context.Entry(vehicle).State = EntityState.Modified;
            var position = new Position
            {
                Latitude = vehicle.Position.Latitude,
                Longitude = vehicle.Position.Longitude,
            };
            _context.Positions.Add(position);
            vehicle.Position = position;
            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!VehicleExists(id))
                {
                    return NotFound();
                }
                else 
                {
                    throw;
                }
            }

            return NoContent();
        }



        /// <summary>
        /// This method allows to add a new vehicle, giving the vehicle and the id.
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        // POST: Vehicles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Vehicle>> PostVehicle([FromBody]Vehicle vehicle)
        {
            _context.Vehicles.Add(vehicle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVehicle", new { id = vehicle.ID }, vehicle);
        }



        /// <summary>
        /// This method allows to delete a vehicle giving its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: Vehicles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Vehicle>> DeleteVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            _context.Vehicles.Remove(vehicle);
            await _context.SaveChangesAsync();

            return vehicle;
        }


        /// <summary>
        /// Locks the vehicle with the given ID
        /// </summary>
        /// <param name="id">The ID of the car</param>
        /// <returns>Status of the car</returns>
        [HttpPut]
        [Route("lock/{id}")]
        [Authorize]  
        public async Task<ActionResult<dynamic>> LockVehicle(int id)
        {
            var cliente = new HttpClient();
            //http request to prototype
            return await ChangeVehicleLockStatus(id, "locked");
            
        }

        [HttpPut]
        [Route("unlock/{id}")]
        [Authorize]
        public async Task<ActionResult<dynamic>> UnlockVehicle(int id)
        {
            return await ChangeVehicleLockStatus(id, "unlocked");
        }

        /// <summary>
        /// This method is used to check if a vehicle already exists or not.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool VehicleExists(int id)
        {
            return _context.Vehicles.Any(e => e.ID == id);
        }

        private async Task<ActionResult<dynamic>> ChangeVehicleLockStatus(int id,string status)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            vehicle.Status = status;
            _context.Entry(vehicle).State = EntityState.Modified;
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehicleExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return new
            {
                status = vehicle.Status,
            };
        }

        public bool IsVehicleAvailable(int vehicleID)
        {
            //We can do a new rental when a vehicle is available, so, we can call this function which has the same which is basically the same thing
            return RentalsController.CanDoNewRental(DateTime.Now, vehicleID, _context);

        }
    }
}
