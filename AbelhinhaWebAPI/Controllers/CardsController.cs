﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Services;
using Microsoft.AspNetCore.Authorization;

namespace AbelhinhaWebAPI.Controllers
{
    public class CardPost
    {
        [Required]
        public string Cardholder { get; set; }

        [Required]
        [RegularExpression("^\\d{3}$")]
        public string SecurityCode { get; set; }

        [Required]
        [RegularExpression("^\\d\\d[/]\\d\\d$")]
        public string ExpireDate { get; set; }

        [Required]
        [RegularExpression("^\\d{16}$")]
        public string CardNumber { get; set; }
    }

    [Route("[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private readonly AbelhinhaWebAPIContext _context;

        public CardsController(AbelhinhaWebAPIContext context)
        {
            _context = context;
        }

        // GET: Cards
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Card>>> GetCards()
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);

            return await _context.Cards.Where(card => card.UserID == userID).ToListAsync();
        }

        // GET: Cards/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Card>> GetCard(int id)
        {

            var card = await _context.Cards.FindAsync(id);


            if (card == null)
            {
                return NotFound();
            }

            return card;
        }

        // PUT: api/Cards/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutCard(int id, Card card)
        {
            if (id != card.ID)
            {
                return BadRequest("NO_CARD");
            }

            _context.Entry(card).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cards
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Card>> PostCard([FromBody]CardPost card)
        {
            int userID = AuthorizationService.GetInstance().GetUserID(this);
            Card cardToUpload = new Card
            {
                CardNumber = card.CardNumber,
                Cardholder = card.Cardholder,
                ExpireDate = card.ExpireDate,
                SecurityCode = card.SecurityCode,
                UserID = userID
            };
            _context.Cards.Add(cardToUpload);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCard", new { id = cardToUpload.ID }, cardToUpload);
        }

        // DELETE: api/Cards/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Card>> DeleteCard(int id)
        {
            var card = await _context.Cards.FindAsync(id);
            if (card == null)
            {
                return NotFound();
            }

            _context.Cards.Remove(card);
            await _context.SaveChangesAsync();

            return card;
        }

        private bool CardExists(int id)
        {
            return _context.Cards.Any(e => e.ID == id);
        }
    }
}
