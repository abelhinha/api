﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AbelhinhaWebAPI.Models;
using AbelhinhaWebAPI.Models.Validations;
using AbelhinhaWebAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;

namespace AbelhinhaWebAPI.Controllers
{
    public class RentalResponse
    {
        public int ID { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public string UserEmail { get; set; }
        public Vehicle Vehicle { get; set; }
    }

    public class StartRentalArg
    {
        public int VehicleID { get; set; }
    }
    public class FinishRentalArg
    {
        public int CardID { get; set; }
    }
    [Route("[controller]")]
    [ApiController]
    public class RentalsController : ControllerBase
    {
        private readonly AbelhinhaWebAPIContext _context;

        public RentalsController(AbelhinhaWebAPIContext context)
        {
            _context = context;
        }

        // GET ROUTE: /rentals
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetRentals()
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);
            return await _context.Rentals.Where(rental => rental.User.ID == userID).Select(rental => new RentalResponse
            {
                ID = rental.ID,
                EndDate = rental.EndDate,
                StartDate = rental.StartDate,
                UserEmail = rental.User.Email,
                Vehicle = rental.Vehicle
            }).ToListAsync();
        }
        
        [HttpGet]
        [Authorize]
        [Route("current")]
        public async Task<ActionResult<RentalResponse>> GetCurrentRentalVehicle()
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);
            var currentRental = await _context.Rentals.OrderByDescending(rental => rental.ID).Where(rental => rental.EndDate == default(DateTime))
                .FirstOrDefaultAsync(rental => rental.User.ID == userID);
            if (currentRental == null) return BadRequest("NO_RENTAL");
            currentRental.EndDate = DateTime.Now; //just to calculate the current price;
            return Ok(new
            {
                ID = currentRental.ID,
                EndDate = currentRental.EndDate,
                StartDate = currentRental.StartDate,
                Vehicle = currentRental.Vehicle,
                CurrentPrice = CalculateRentalPrice(currentRental)
            });

        }

        // GET: Rentals/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Rental>> GetRental(int id)
        {
            var rental = await _context.Rentals.FindAsync(id);

            if (rental == null)
            {
                return NotFound();
            }

            return rental;
        }

        [HttpGet]
        [Route("/activate")]
        public async Task<ActionResult<Rental>> TestComms()
        {
            //TODO REMOVE
            NotificationService.GetInstance().SendNotification("car-available", "Volkswagen Golf caga fumo");
            return NoContent();

        }

        /// <summary>
        /// PUT /rentals/finish/{id}
        /// Gets the id of a rental and finishes it
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rental"></param>
        /// <returns></returns>
        [Route("finish")]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> FinishRental()
        {
            var userID = AuthorizationService.GetInstance().GetUserID(this);
            var  rental = await _context.Rentals.OrderByDescending(rental => rental.ID).FirstOrDefaultAsync(rental => rental.User.ID == userID);
            if (rental == default(Rental)) return BadRequest("NO_RENTALS");
            if (rental.EndDate != default(DateTime)) return BadRequest("NO_UNFINISHED_RENTALS");
            
            var payment = new Payment
            {
                Rental = rental
            };

            rental.EndDate = DateTime.Now;
            var price = CalculateRentalPrice(rental);
            payment.Price = price;
            payment.Status = payment.State.ToString(); //aldrabice
            _context.Payments.Add(payment);
            _context.Entry(rental).State = EntityState.Modified;
            var carPosition = rental.Vehicle.Position;
            //var carAddress = await LocationService.GetInstance().GetAddress(carPosition.Latitude, carPosition.Longitude);
            //rental.Vehicle.Address = carAddress; TODO NOT WORKING
            _context.Entry(rental.Vehicle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {   
                Console.WriteLine("Error saving rental?");
            }
            NotificationService.GetInstance().SendNotification("car-available", rental.Vehicle.ToString());
            return Ok(new
            {
                TotalPrice = price,
                PaymentID = payment.ID
            });
        }

        // POST: Rentals
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("start")]
        [Authorize]
        public async Task<ActionResult<Rental>> StartRental([FromBody]StartRentalArg vehicleId)
        {
            var uid = AuthorizationService.GetInstance().GetUserID(this);
            var user = await _context.Users.SingleOrDefaultAsync(User => User.ID == uid);
            if (user.Cards == default(List<Card>)) return BadRequest("NO_CARD");
            var pendingPayment = await _context.Payments.Where(payment => payment.Rental.User.ID == uid && payment.Status != "Pago").FirstOrDefaultAsync();
            if (pendingPayment != default(Payment)) return BadRequest("PENDING_PAYMENT");
            var rentalUpload = new Rental
            {
                User = user,
                Vehicle = await _context.Vehicles.SingleOrDefaultAsync(Vehicle => Vehicle.ID == vehicleId.VehicleID),
                StartDate = DateTime.Now
            };
            if (!CanUserCreateNewRental(_context,this)) return BadRequest("ONLY_ONE_RENTAL_AT_A_TIME");
            if (!CanDoNewRental(rentalUpload.StartDate, vehicleId.VehicleID, _context)) return BadRequest("ONGOING_RENTAL");


            _context.Rentals.Add(rentalUpload);
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: Rentals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Rental>> DeleteRental(int id)
        {
            var rental = await _context.Rentals.FindAsync(id);
            if (rental == null)
            {
                return NotFound();
            }

            _context.Rentals.Remove(rental);
            await _context.SaveChangesAsync();

            return rental;
        }

        private bool RentalExists(int id)
        {
            return _context.Rentals.Any(e => e.ID == id);
        }


        private double CalculateRentalPrice(Rental rental)
        {
            var timeSpan = rental.EndDate.Subtract(rental.StartDate);
            var price = Math.Floor(timeSpan.TotalMinutes) * rental.Vehicle.RentPrice;
            return Math.Round(price,2);
        }


        public static bool CanDoNewRental(DateTime value, int vehicleID, AbelhinhaWebAPIContext aContext)
        {
            var lastRental = aContext.Rentals.OrderByDescending(rental => rental.ID).FirstOrDefault(rental => rental.Vehicle.ID == vehicleID);
            if (lastRental == default(Rental)) return true;
            if (lastRental.EndDate == default(DateTime)) return false;
            if (value.CompareTo(lastRental.StartDate) > 0 && value.CompareTo(lastRental.EndDate) > 0) return true;
            return false;
        }

        public static bool CanUserCreateNewRental(AbelhinhaWebAPIContext aContext, ControllerBase aController)
        {
            var uid = AuthorizationService.GetInstance().GetUserID(aController);
            var lastRental = aContext.Rentals.OrderByDescending(rent => rent.ID).Where(rent => rent.EndDate == default(DateTime))
                .FirstOrDefault(rent => rent.User.ID == uid);
            return (lastRental == default(Rental));
            //if it is default means there was no rental found, hence it can create
        }
    }
    

}
