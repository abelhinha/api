﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace AbelhinhaWebAPI
{
    public static class IWebHostBuilderExtension
    {

        public static IWebHostBuilder UsePort(this IWebHostBuilder builder)
        {
            var Port = Environment.GetEnvironmentVariable("PORT");
            if (string.IsNullOrEmpty(Port))
            {
                return builder;
            }

            return builder.UseUrls($"http://*:{Port}");
        }
    }
}
