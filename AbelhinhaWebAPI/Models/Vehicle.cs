﻿using System.ComponentModel;
using AbelhinhaWebAPI.Services;
using SQLitePCL;

namespace AbelhinhaWebAPI.Models
{
    public class Vehicle
    {
        private string _address { get; set; }
        public int ID { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int HorsePower { get; set; }
        public string FuelType{ get; set; }
        public string License{ get; set; }
        public int Seats { get; set; }
        public string Color { get; set; }
        public float RentPrice { get; set; }
        public float Speed { get; set; }
        public virtual Position Position { get; set; }
        public string Image { get; set; }

        [DefaultValue("locked")]
        public string Status { get; set; }

        public string Address { get; set; }
    }
}