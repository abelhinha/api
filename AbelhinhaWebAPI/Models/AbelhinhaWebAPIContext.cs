﻿using AbelhinhaWebAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AbelhinhaWebAPI.Models
{
    public class AbelhinhaWebAPIContext : DbContext
    {
        public AbelhinhaWebAPIContext(DbContextOptions<AbelhinhaWebAPIContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<FTPLocation> FtpLocations { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Payment> Payments { get; set; }



    }
}