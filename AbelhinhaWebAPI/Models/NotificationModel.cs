﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Models
{
    public class NotificationModel
    {
        public string Type { get; set; }
        public string Notification { get; set; }
    }
}
