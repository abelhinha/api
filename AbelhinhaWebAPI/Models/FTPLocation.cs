﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Models
{
    public class FTPLocation
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Location { get; set; }
        public virtual User UID { get; set; }
    }
}
