﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Models.Validations;

namespace AbelhinhaWebAPI.Models
{
    public class Rental
    {
        public int ID { get; set; }
        
        [Required]
        public virtual User User { get; set; }
        
        [Required]
        public virtual Vehicle Vehicle { get; set; }
        
        public virtual DateTime StartDate { get; set; }

        [DateIsGreaterThanDateOrNull("StartDate")]
        public virtual DateTime EndDate { get; set; }
    }
}
