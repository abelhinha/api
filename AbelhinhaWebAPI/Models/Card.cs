﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Castle.Components.DictionaryAdapter;

namespace AbelhinhaWebAPI.Models
{
    public class Card
    {
        public int ID { get; set; }
        
        [Required]
        public string Cardholder { get; set; }

        [Required]
        [StringLength(3)]
        public string SecurityCode { get; set; }

        [Required]
        [RegularExpression("\\d\\d[/]\\d\\d")]
        public string ExpireDate { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public int UserID { get; set; }
    }
}
