﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using AbelhinhaWebAPI.Patterns;

namespace AbelhinhaWebAPI.Models
{
    public class Payment
    {
        public int ID { get; set; }
        
        [Required]
        public virtual Rental Rental { get; set; }

        public virtual Card Card { get; set; }

        [Required]
        public double Price { get; set; }

        public string Status { get; set; }

        public PaymentState State {get => _state; }
        private PaymentState _state;


        public Payment()
        {
            _state = new PaymentPendingState(this);
        }

        public void ChangeState(PaymentState state)
        {
            this._state = state;
        }

        public void Paid()
        {
            _state.Paid();
        }

        public void Pending()
        {
            _state.Pending();
        }

        public void Error()
        {
            _state.Pending();
        }
        
    }
}
