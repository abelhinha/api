﻿using System;
using System.Buffers.Text;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace AbelhinhaWebAPI.Models.Validations
{
    public class RegistrationModel
    {

        [Required]
        public string Username { get; set; }
        
        [EmailAddress]
        [EmailUnique]
        [Required]
        public string Email { get; set; }

        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        [MaxLength(3)]
        [MinLength(3)]
        [Required]
        public IFormFile[] Files { get; set; }


    }
}