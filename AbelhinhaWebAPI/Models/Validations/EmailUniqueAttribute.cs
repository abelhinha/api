﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Models.Validations
{
    /// <summary>
    /// Checks if the email attribute is unique.
    /// </summary>
    public class EmailUniqueAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var _context = (AbelhinhaWebAPIContext)validationContext.GetService(typeof(AbelhinhaWebAPIContext)); 
            var entity = _context.Users.SingleOrDefault(u => u.Email == value.ToString());

            if (entity != null)
            {
                return new ValidationResult(GetErrorMessage());
            }
            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return $"Email is already in use.";
        }
    }
}
