﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Models.Validations
{
    public class RentalModel
    {
        public int ID { get; set; }

        [Required]
        public int UserID { get; set; }

        [Required]
        public int VehicleID { get; set; }

        public virtual string StartDate { get; set; }

        [DateIsGreaterThanDateOrNull("StartDate")]
        public virtual string EndDate { get; set; }
    }
}
