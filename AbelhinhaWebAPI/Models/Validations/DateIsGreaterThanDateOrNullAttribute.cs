﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace AbelhinhaWebAPI.Models.Validations
{
    /// <summary>
    /// Checks if the email attribute is unique.
    /// </summary>
    public class DateIsGreaterThanDateOrNullAttribute : ValidationAttribute
    {
        private readonly string _otherProperty;

        public DateIsGreaterThanDateOrNullAttribute(string aProperty)
        {
            _otherProperty = aProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null) return ValidationResult.Success;
            var otherProperty = validationContext.ObjectType.GetProperty(_otherProperty);
            if (otherProperty == null)
                return new ValidationResult(string.Format(CultureInfo.CurrentCulture, "Unknown property {0}", new[] { otherProperty }));

            var propertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null) as string;
            
            var otherDate = DateTime.Parse(propertyValue);
            var valueDate = DateTime.Parse(value as string);
            if (valueDate.CompareTo(otherDate) == 1)
                return ValidationResult.Success;
            return  new ValidationResult("End date is earlier than start date");

        }

        public string GetErrorMessage(string email)
        {
            return $"The start date is greater than the end date is already in use.";
        }
    }
}
