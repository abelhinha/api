﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AbelhinhaWebAPI.Models
{
    public class Position
    {
        public int ID { get; set; }
        public float  Latitude { get; set; }
        public float Longitude { get; set; }

    }
}
