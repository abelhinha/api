FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build-env
WORKDIR /app
 
# Copy csproj and restore as distinct layers
COPY AbelhinhaWebAPI/*.csproj ./
RUN dotnet restore
 
# Copy everything else and build
COPY AbelhinhaWebAPI/. ./
RUN dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0
WORKDIR /app
COPY --from=0 /app/out .
CMD dotnet AbelhinhaWebAPI.dll